import simplerun

from setuptools import setup

setup(
    name='simplerun',
    version=simplerun.__version__,
    author='netspyer',
    packages=['simplerun'],
    license='GPL v2')
